//
// Created by hasee on 2021/4/30.
//

#include <iostream>
#include <opencv2/xfeatures2d.hpp>
#include "../util/Reconstruction.h"
#include "../util/FileNames.h"

using namespace std;
using namespace cv;

void testUtil() {
    FileNames imageFiles(
            {
                    "../image/14/IMG_2779.JPG",
                    "../image/14/IMG_2780.JPG"
            });

    Reconstruction r(imageFiles.paths());
    r.feature_extract();
    r.feature_match();
}

void testFeature() {
    vector<string> files = {
            "../image/14/IMG_2779.JPG",
            "../image/14/IMG_2780.JPG"
    };

    vector<Mat> images = {
            imread(files[0]),
            imread(files[1])
    };

    for (auto &image : images) {
        resize(image, image, Size(image.cols / 6, image.rows / 6));
    }

    auto surf = xfeatures2d::SURF::create(800);
    auto sift = SIFT::create();
    vector<vector<KeyPoint>> keyPoints(images.size());
    vector<Mat> descriptors(images.size());
    for (int i = 0; i < images.size(); ++i) {
        sift->detectAndCompute(images[i], noArray(), keyPoints[i], descriptors[i]);
    }


    FlannBasedMatcher matcher;
    vector<DMatch> matches, goodMatches;
    matcher.match(descriptors[0], descriptors[1], matches);

    cout
    << "key points: " << keyPoints.size()
    << "match size: " << matches.size()
    << endl;

    for (int i = 0; i < matches.size() - 1; ++i) {
        if (matches[i].distance < 0.4 * matches[i + 1].distance)
            goodMatches.emplace_back(matches[i]);
    }

    Mat result;
    drawMatches(images[0], keyPoints[0], images[1], keyPoints[1], goodMatches, result);
//    resize(result, result, Size(result.cols / 6, result.rows / 6));
    imshow("match", result);
    waitKey();

}

bool find_transform(Mat &K, vector<Point2d> &p1, vector<Point2d> &p2, Mat &R, Mat &T, Mat &mask) {
    double focal_length = 0.5 * (K.at<double>(0) + K.at<double>(4));
    Point2d principle_point(K.at<double>(2), K.at<double>(5));

    Mat E = findEssentialMat(p1, p2, focal_length, principle_point, RANSAC, 0.999, 1.0, mask);
    if (E.empty())
        return false;

    double feasible_count = countNonZero(mask);
    cout << feasible_count << " -in- " << p1.size() << endl;

    if (feasible_count <= 15 || (feasible_count / p1.size()) < 0.6)
        return false;

    int pass_count = recoverPose(E, p1, p2, R, T, focal_length, principle_point, mask);

    if (pass_count / feasible_count < 0.7)
        return false;

    return true;
}

void reconstruct(Mat &K, Mat &R, Mat &T, vector<Point2d> &p1, vector<Point2d> &p2, Mat &structure) {
    Mat proj1(3, 4, CV_32FC1);
    Mat proj2(3, 4, CV_32FC1);

    proj1(Range(0, 3), Range(0, 3)) = Mat::eye(3, 3, CV_32FC1);
    proj1.col(3) = Mat::zeros(3, 1, CV_32FC1);

    R.convertTo(proj2(Range(0, 3), Range(0, 3)), CV_32FC1);
    T.convertTo(proj2.col(3), CV_32FC1);

    Mat fK;
}

int main() {

    testFeature();

    return 0;
}