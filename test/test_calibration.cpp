//
// Created by hasee on 2021/5/7.
//

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main() {

    Mat img = imread("../result/picture5.JPG", IMREAD_GRAYSCALE);
    resize(img, img, Size(img.cols / 4, img.rows / 4));
    Size size(9, 6);
    Mat ptvec;
    bool found = findChessboardCorners(img, size, ptvec, CALIB_CB_ADAPTIVE_THRESH);
    if (found) {
        drawChessboardCorners(img, size, ptvec, true);
        imshow("", img);
        waitKey();
    } else
        cout << "corner not found" << endl;

    calibrateCamera(object_points, image_points, Size(features[0].image.cols, features[0].image.rows), K, distCoeffs, noArray(), noArray(), noArray(), noArray(), noArray());

    return 0;
}
