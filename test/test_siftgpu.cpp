//
// Created by hasee on 2021/5/3.
//

#include <opencv2/opencv.hpp>
#include <GL/gl.h>
#include "../other/SiftGPU/include/SiftGPU.h"

using namespace std;
using namespace cv;

int main(int argc, char** argv) {
    SiftGPU sift;
    char* myargv[4] = {"-fo", "-1", "-v", "1"};
    sift.ParseParam(4, myargv);

    int support = sift.CreateContextGL();
    if (support != SiftGPU::SIFTGPU_FULL_SUPPORTED) {
        cerr << "SiftGPU is not supported" << endl;
        return 2;
    }

    cout << "running sift" << endl;
    sift.RunSIFT("../image/14/IMG_2779.JPG");

    int num = sift.GetFeatureNum();
    cout << "Feature number = " << num << endl;
    vector<float> descriptors(128 * num);
    vector<SiftGPU::SiftKeypoint> keys(num);

    sift.GetFeatureVector(&keys[0], &descriptors[0]);

}