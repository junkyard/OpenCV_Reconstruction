//
// Created by hasee on 2021/2/10.
//

#include <iostream>
//#include "ceres/ceres.h"
#include <opencv2/opencv.hpp>
#include "../util/Reconstruction.h"
#include "../util/PointCloud.h"
#include "../util/FileNames.h"

using namespace std;

struct ImageDir {
    QString dir = "G:/OneDrive - cjlu.edu.cn/3D Reconstruction/OpenCV_Reconstruction/image/";
    QString name = "IMG_%1.JPG";

    QString directory(int number) {
        return dir + QString("%1/").arg(number);
    }

    ImageDir() = default;
    ImageDir(QString dir, QString name) {
        this->dir = dir;
        this->name = name;
    }
};

ImageDir image;
ImageDir tower("G:/OneDrive - cjlu.edu.cn/3D Reconstruction/data_tower/", "San_Francisco_%1.jpg");

string resultYaml1 = "../image/potato.yml";

void testImageShow() {
    cv::Mat photo = cv::imread("../image/1/IMG_2579.JPG");
    cv::imshow("test", photo);
    cv::waitKey();
}

void testCompute(QString dir=image.dir + "1/", const QString &name=image.name, int number=2579, int num=4) {
    QList<QString> images;
    for (int i = 0; i < num; i++) {
        images.emplace_back(dir + name.arg(number + i));
    }
    FileNames imageNames(images);
//    reverse(begin(images), end(images));

    rc::Reconstruction r(imageNames);
    r.extractor.type = rc::Extractor::SIFT;
    r.matcher.type = rc::Matcher::Flann;
    r.matcher.knn = true;
    r.run(0);
}

void testShow(string yaml=resultYaml1) {
    PointCloud s(yaml);
    s.show();
}

void testFileNames() {
    QList<QString> Qfiles1 = {
            "hao/good/1.jpg",
            "hao/good/2.jpg"
    }, Qfiles2 = {
            "huai/bad/1.jpg",
            "huai/bad/2.jpg"
    };

    FileNames files1(Qfiles1), files2(Qfiles2);

    cout << "files 1: " << files1 << endl;
    cout << "files 2: " << files2 << endl;
    cout << "files 1 + 2: " << files1 + files2 << endl;
    files1 += files2;
    cout << "files 1 += 2: " << files1 << endl;
}

int main() {


    return 0;
}