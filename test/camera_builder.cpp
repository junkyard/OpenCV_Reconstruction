//
// Created by hasee on 2021/5/25.
//
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

vector <Matx33d> Ks = {
        //初始
        Matx33d(2759.48, 0, 1520.69,
                0, 2764.16, 1006.81,
                0, 0, 1),
        //iPhone 7
        Matx33d(649.62668716, 0, 395.66659444,
                0, 647.12163354, 293.60311235,
                0, 0, 1),
        //iPhone 11 近
        Matx33d(389.4301231494331, 0, 648.1958304734038,
                0, 231.7740368676911, 455.2059962378937,
                0, 0, 1),
        //iPhone 11 中
        Matx33d(3090.207692934584, 0, 403.2093661090428,
                0, 7611.568338434958, 627.0893381629936,
                0, 0, 1),
        Matx33d(800, 0, 400,
                0, 800, 255,
                0, 0, 1),
        Matx33d(5475.93829155217, 0, 1513.385362111364,
                0, 3096.454399177298, 1457.345047413419,
                0, 0, 1)
};

vector<vector<double>> K_paras = {
        {2759.48, 2764.16, 1520.69, 1006.81},
        {5475.93829155217, 3096.454399177298, 1513.385362111364, 1457.345047413419},
        {2764.16, 2759.48, 1513.385362111364, 1457.345047413419},
        {2000, 1900, 1520.69, 1006.81}
};

void save() {
    FileStorage fs("../camera/camera matrix.yml", FileStorage::WRITE);

    if (fs.isOpened()) {
        fs << "camera matrix" << K_paras;
        fs.release();
    }
}

void loadTo(vector<Matx33d> &K_matrix) {

    FileStorage fs("../camera/camera matrix.yml", FileStorage::READ);

    if (fs.isOpened()) {
        fs["camera matrix"] >> K_matrix;
        fs.release();
    }
}

void loadTo(vector<vector<double>> &K_para) {
    FileStorage fs("../camera/camera matrix.yml", FileStorage::READ);

    if (fs.isOpened()) {
        fs["camera matrix"] >> K_para;
        fs.release();
    }
}

Mat test_image() {
    return imread("../image/1/IMG_2579.JPG");
}

void test_read() {
    ifstream fs("../camera/camera matrix.txt");
    vector<vector<double>> a;
    int camera = 0;
    if (fs) {
        while (!fs.eof()) {
            vector<double> b(4);
            fs >> b[0] >> b[1] >> b[2] >> b[3];
            a.emplace_back(b);
        }
        fs.close();
    }
    cout << a[3][1] << endl;
}

int main() {

//    save();

//    vector<vector<double>> K_para;
//    loadTo(K_para);
//    int i = 0;
//    Matx33d K(K_para[i][0], 0, K_para[i][2],
//              0, K_para[i][1], K_para[i][3],
//              0, 0, 1);
//    cout << K << endl;

    test_read();

    return 0;
}
