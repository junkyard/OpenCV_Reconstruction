//
// Created by hasee on 2021/3/7.
//

#include "Reconstruction.h"
#include <iostream>
#include <fstream>
#include <utility>
//#include <opencv2/sfm.hpp>
#include <opencv2/xfeatures2d.hpp>

#include <ceres/ceres.h>
#include <ceres/rotation.h>

rc::Reconstruction::Reconstruction(FileNames files)
        : imageNames(std::move(files)), features(), matches_for_all() {
    cout << endl << "total " << imageNames.size() << " images" << endl;
}

rc::Reconstruction::Reconstruction()
        : features(), matches_for_all() {}

PointCloud rc::Reconstruction::run(int camera) {

    vector<vector<double>> parameters;

    ifstream fs("../camera/camera matrix.txt");

    if (fs) {
        while (!fs.eof()) {
            vector<double> p(4);
            fs >> p[0] >> p[1] >> p[2] >> p[3];
            parameters.emplace_back(p);
        }
        fs.close();
    }

    Mat K = Mat(Matx33d(
            parameters[camera][0], 0, parameters[camera][2],
            0, parameters[camera][1], parameters[camera][3],
            0, 0, 1
    ));

//    Mat K = Mat(Ks[camera]);

    cout << K << endl;

    extractor.extract(features, imageNames);
    matcher.match(features, matches_for_all);

    PointCloud cloud;

    if (!init_structure(K, cloud))
        return cloud;

    for (int i = init_structure_end; i < matches_for_all.size(); ++i) {
        vector<Point3d> object_points;
        vector<Point2f> image_points;
        Mat r, R, T;

        get_objpoints_and_imgpoints(
                matches_for_all[i],
                features[i].structure_idx,
                cloud.points,
                features[i + 1].key_points,
                object_points,
                image_points
        );

        cout << "object points: " << object_points.size() << endl
             << "image points: " << image_points.size() << endl;
        cout << "solve PnP ";
//        求解变换矩阵
        if (solvePnPRansac(object_points, image_points, K, noArray(), r, T, false, 500)) {
            cout << "succeed" << endl;
        } else {
            cout << "failed" << endl;
            continue;
        }

//        cout << "r: " << r << endl;

//        将旋转向量转换为旋转矩阵
        Rodrigues(r, R);
//        cout << "R: " << R << endl;
        cloud.rotations.push_back(R);
        cloud.motions.push_back(T);

        vector<Point2d> p1, p2;
        vector<Vec3b> c1, c2;

        get_matched_points(features[i].key_points, features[i + 1].key_points, matches_for_all[i], p1, p2);
        get_matched_colors(features[i].colors, features[i + 1].colors, matches_for_all[i], c1, c2);

        vector<Point3d> next_points;
        cout << "rotations: " << cloud.rotations.size() << endl
             << "motions: " << cloud.motions.size() << endl;
        reconstruct(K, cloud.rotations[i], cloud.motions[i], R, T, p1, p2, next_points);

        fusion_structure(matches_for_all[i],
                         features[i], features[i + 1], cloud.points,
                         next_points, cloud.colors, c1);
    }

//    PointCloud pointCloud1(K, cloud.rotations, cloud.motions, cloud.points, cloud.colors);
//    pointCloud1.show();

    if (bundler.enabled) {
        Mat intrinsic(Matx41d(K.at<double>(0, 0), K.at<double>(1, 1), K.at<double>(0, 2), K.at<double>(1, 2)));
        vector<Mat> extrinsics;
        for (size_t i = 0; i < cloud.rotations.size(); ++i) {
            Mat extrinsic(6, 1, CV_64FC1);
            Mat r;
            Rodrigues(cloud.rotations[i], r);

            r.copyTo(extrinsic.rowRange(0, 3));
            cloud.motions[i].copyTo(extrinsic.rowRange(3, 6));

            extrinsics.push_back(extrinsic);
        }

        bundler.adjust(intrinsic, extrinsics, features, cloud.points);
    }
    PointCloud pointCloud(K, cloud.rotations, cloud.motions, cloud.points, cloud.colors);
    pointCloud.show();
    return pointCloud;
}

void rc::Reconstruction::feature_extract() {
    extractor.extract(features, imageNames);
}

void rc::Reconstruction::feature_match() {
    matcher.match(features, matches_for_all);
}

bool rc::Reconstruction::init_structure(Mat K, PointCloud &cloud) {
    vector<Point2d> p1, p2;
    vector<Vec3b> c1, c2;

//    旋转矩阵 平移矩阵
    Mat R, T;
    Mat mask;

    init_structure_end = 0;

    do {
        if (++init_structure_end >= features.size()) {
            cout << "no suitable initial structure found" << endl;
            return false;
        }
        cout << "init end: " << init_structure_end << endl;
        get_matched_points(features[init_structure_end - 1].key_points, features[init_structure_end].key_points,
                           matches_for_all[init_structure_end - 1], p1, p2);
        get_matched_colors(features[init_structure_end - 1].colors, features[init_structure_end].colors,
                           matches_for_all[init_structure_end - 1], c1, c2);
    } while (!find_transform(K, p1, p2, R, T, mask));

    cout << "init with image pairs " << init_structure_end - 1 << "-" << init_structure_end << endl;

    maskout_points(p1, mask);
    maskout_points(p2, mask);
    cloud.colors = c1;
    maskout_colors(cloud.colors, mask);

//    初始化3X3矩阵的单位矩阵
    Mat R0 = Mat::eye(3, 3, CV_64FC1);
//    初始化1X3矩阵
    Mat T0 = Mat::zeros(3, 1, CV_64FC1);

    reconstruct(K, R0, T0, R, T, p1, p2, cloud.points);

    cloud.rotations = {R0, R};
    cloud.motions = {T0, T};

    for (auto &feature : features) {
        feature.structure_idx.resize(feature.key_points.size(), -1);
    }

    int idx = 0;
    vector<DMatch> &matches = matches_for_all[init_structure_end - 1];
    for (int i = 0; i < matches.size(); ++i) {
        if (mask.at<uchar>(i) == 0)
            continue;

        features[init_structure_end - 1].structure_idx[matches[i].queryIdx] = idx;
        features[init_structure_end].structure_idx[matches[i].trainIdx] = idx;
        ++idx;
    }

    return true;
}

void rc::Reconstruction::get_objpoints_and_imgpoints(vector<DMatch> &matches, vector<int> &struct_indices,
                                                     vector<Point3d> &structure, vector<KeyPoint> &key_points,
                                                     vector<Point3d> &object_points, vector<Point2f> &image_points) {

    object_points.clear();
    image_points.clear();

    for (auto &match : matches) {
        int query_idx = match.queryIdx;
        int train_idx = match.trainIdx;

        int struct_idx = struct_indices[query_idx];
        if (struct_idx < 0)
            continue;

        object_points.emplace_back(structure[struct_idx]);
        image_points.emplace_back(key_points[train_idx].pt);
    }
}


void rc::Reconstruction::get_matched_points(
        vector<KeyPoint> &p1, vector<KeyPoint> &p2,
        const vector<DMatch> &matches,
        vector<Point2d> &out_p1, vector<Point2d> &out_p2) {

    out_p1.clear();
    out_p2.clear();

    for (auto &match : matches) {
//        DMatch query的点与train的点相匹配
        out_p1.emplace_back(p1[match.queryIdx].pt);
        out_p2.emplace_back(p2[match.trainIdx].pt);
    }
}

void
rc::Reconstruction::get_matched_colors(vector<Vec3b> &c1, vector<Vec3b> &c2, vector<DMatch> &matches,
                                       vector<Vec3b> &out_c1,
                                       vector<Vec3b> &out_c2) {
    out_c1.clear();
    out_c2.clear();
    for (auto &match : matches) {
        out_c1.emplace_back(c1[match.queryIdx]);
        out_c2.emplace_back(c2[match.trainIdx]);
    }
}

void
rc::Reconstruction::reconstruct(Mat &K, Mat &R1, Mat &T1, Mat &R2, Mat &T2, vector<Point2d> &p1, vector<Point2d> &p2,
                                vector<Point3d> &structure) {

    cout << "reconstructing" << endl;

    cout << "R1:\n" << R1 << endl;
    cout << "T1:\n" << T1 << endl;
    cout << "R2:\n" << R2 << endl;
    cout << "T2:\n" << T2 << endl;

    Mat proj1(3, 4, CV_32FC1);
    Mat proj2(3, 4, CV_32FC1);

    R1.convertTo(proj1(Range(0, 3), Range(0, 3)), CV_32FC1);
    T1.convertTo(proj1.col(3), CV_32FC1);

    R2.convertTo(proj2(Range(0, 3), Range(0, 3)), CV_32FC1);
    T2.convertTo(proj2.col(3), CV_32FC1);

    cout << "proj1:\n" << proj1 << endl;
    cout << "proj2:\n" << proj2 << endl;

//    浮点型内参
    Mat fK;
    K.convertTo(fK, CV_32FC1);
    proj1 = fK * proj1;
    proj2 = fK * proj2;

    cout << "fK:\n" << fK << endl;
    cout << "proj1:\n" << proj1 << endl;
    cout << "proj2:\n" << proj2 << endl;

    Mat s;

//    三角重建
    triangulatePoints(proj1, proj2, p1, p2, s);

    cout << "triangulate points done" << endl;

    structure.clear();
    structure.reserve(s.cols);
    for (int i = 0; i < s.cols; ++i) {
        Mat_<float> col = s.col(i);
        col /= col(3);
        structure.emplace_back(col(0), col(1), col(2));
    }

    cout << "reconstruction done" << endl;
}

void
rc::Reconstruction::fusion_structure(vector<DMatch> &matches, ImageFeature &left, ImageFeature &right,
                                     vector<Point3d> &structure, vector<Point3d> &next_structure, vector<Vec3b> &colors,
                                     vector<Vec3b> &next_colors) {

    for (int i = 0; i < matches.size(); ++i) {

        int query_idx = matches[i].queryIdx;
        int train_idx = matches[i].trainIdx;

        int struct_idx = left.structure_idx[query_idx];
        if (struct_idx >= 0) {
            right.structure_idx[train_idx] = struct_idx;
            continue;
        }

        structure.emplace_back(next_structure[i]);
        colors.emplace_back(next_colors[i]);
        left.structure_idx[query_idx] = structure.size() - 1;
        right.structure_idx[train_idx] = structure.size() - 1;
    }
}

bool rc::Reconstruction::find_transform(Mat &K, vector<Point2d> &p1, vector<Point2d> &p2, Mat &R, Mat &T, Mat &mask) {

//    焦距
    double focal_length = 0.5 * (K.at<double>(0) + K.at<double>(4));
    cout << "focal_length: " << focal_length << endl;
//    focal_length = 2912.00; //焦距
//    光心, 焦点
    Point2d principle_point(K.at<double>(2), K.at<double>(5));

//    E 本征矩阵
//    mask 蒙版，1为目标，0为背景
    Mat E = findEssentialMat(p1, p2, focal_length, principle_point, RANSAC, 0.999, 1.0, mask);

    if (E.empty())
        return false;

//    可用点，目标点
    double feasible_count = countNonZero(mask);
    cout << (int) feasible_count << " -in- " << p1.size() << endl;

    if (feasible_count <= 15 || (feasible_count / p1.size()) < 0.6) {
        cout << "no enough feasible points" << endl;
        return false;
    }

    int pass_count = recoverPose(E, p1, p2, R, T, focal_length, principle_point, mask);

//    if (feasible_count <= 15 || (feasible_count / p1.size()) < 0.6) {
//        cout << "found failed: 1" << endl;
//        return false;
//    }

    if (((double) pass_count) / feasible_count < 0.7) {
        cout << "found failed: 2" << endl;
        return false;
    }

    return true;
}

//找出目标
void rc::Reconstruction::maskout_points(vector<Point2d> &p1, Mat &mask) {
    vector<Point2d> p1_copy = p1;
    p1.clear();

    for (int i = 0; i < mask.rows; ++i) {
        if (mask.at<uchar>(i) > 0)
            p1.emplace_back(p1_copy[i]);
    }
}

//找出颜色
void rc::Reconstruction::maskout_colors(vector<Vec3b> &p1, Mat &mask) {
    vector<Vec3b> p1_copy = p1;
    p1.clear();

    for (int i = 0; i < mask.rows; ++i) {
        if (mask.at<uchar>(i) > 0)
            p1.push_back(p1_copy[i]);
    }
}

ostream &rc::operator<<(ostream &out, ImageFeature &feature) {

    switch (feature.status) {
        case rc::ImageFeature::done:
            out << "details of image feature: " << endl
                << "image: " << feature._image.name() << endl
//                << "image: " << feature.image().cols << "X" << feature.image().rows << endl
                << "key points: " << feature.key_points.size() << endl
                << "descriptor: " << feature.descriptor.cols << "X" << feature.descriptor.rows << endl;
            break;
        case rc::ImageFeature::invalid:
            out << "feature extracting failed" << endl;
            break;
    }

    return out;
}

ostream &rc::operator<<(ostream &out, const ImageFeature::Status &status) {

    switch (status) {
        case 0:
            out << "invalid";
            break;
        case 1:
            out << "done";
            break;
    }

    return out;
}

void rc::ImageFeature::color_record(Mat &image) {
    for (auto &keyPoint : key_points) {
        auto p = keyPoint.pt;
        colors.emplace_back(image.at<Vec3b>(p.y, p.x));
    }
}

//bool rc::ImageFeature::empty() {
//    return image().empty();
//}

rc::ImageFeature::ImageFeature() = default;

rc::ImageFeature::ImageFeature(const string &image_name, const Ptr<Feature2D> &featurePtr)
        : _image(image_name) {

    if (load(saved_path()))
        return;

    Mat im = image();
    featurePtr->detectAndCompute(im, noArray(), key_points, descriptor);

    if (key_points.size() > 10) {
        color_record(im);
        status = done;
        save(saved_path());
    } else {
        cout << "key points detect failed" << endl;
    }
}

string rc::ImageFeature::saved_path() {
    return _image.directory() + "feature " + _image.name() + ".yml";
}

void rc::ImageFeature::save(const string &path) const {
    cout << "save feature to " << path << endl;
    FileStorage fs(path, FileStorage::WRITE);
    if (fs.isOpened()) {
        fs << "Key Points" << key_points;
        fs << "Descriptor" << descriptor;
        fs << "Colors" << colors;
        fs.release();
    }
}

bool rc::ImageFeature::load(const string &path) {
    FileStorage fs(path, FileStorage::READ);
    if (fs.isOpened()) {
        cout << "load feature from " << path << endl;
        fs["Key Points"] >> key_points;
        fs["Descriptor"] >> descriptor;
        fs["Colors"] >> colors;
        fs.release();
        status = done;
        return true;
    }
    return false;
}

Mat rc::ImageFeature::image() const {
    Mat image = imread(_image.path());
    return image;
}

rc::Extractor::Extractor() :
        type(SIFT), surf_minHessian(1500) {}

//提取特征
void rc::Extractor::extract(vector<ImageFeature> &features, const FileNames &imageNames) {

    features.clear();

    switch (type) {
        case SIFT:
            cout << "SIFT Extractor" << endl;
            featurePtr = cv::SIFT::create(0, 3, 0.04, 10);
            break;
        case SURF:
            cout << "SURF Extractor " << endl;
            featurePtr = xfeatures2d::SURF::create(surf_minHessian, 4, 3, true, true);
            break;
    }

    for (auto &image : imageNames.files()) {
        cout << "Extracting features: " << image.name() << " ... " << endl;
        ImageFeature imageFeature(image.path(), featurePtr);

        if (imageFeature.status == ImageFeature::done)
            features.emplace_back(imageFeature);
        cout << imageFeature.status << endl;
        cout << imageFeature << endl;
    }
}

rc::Matcher::Matcher() :
        type(Flann), knn(true), draw(false), i1(), i2(), f1() {
    init();
}

//匹配特征
void rc::Matcher::match(vector<ImageFeature> &features, vector<vector<DMatch>> &matches_for_all) {
    matches_for_all.clear();

    min_dist = FLT_MAX;

    switch (type) {
        case BruteForce:
            descriptorMatcher = new BFMatcher;
            cout << "Brute Match: ";
            break;
        case Flann:
            descriptorMatcher = new FlannBasedMatcher;
            cout << "Flann Match: ";
            break;
    }

    for (int i = 0; i < features.size() - 1; ++i) {
        cout << "Matching images " << i << " - " << i + 1 << endl;
        vector<DMatch> matches;

        if (knn) {
            cout << "knn... ";
            knnMatch(features[i], features[i + 1], matches);
        } else {
            oneMatch(features[i], features[i + 1], matches);
        }

        cout << "matches after filtering: " << matches.size() << endl;

        matches_for_all.emplace_back(matches);
    }

    if (draw) {
        for (int i = 0; i < features.size() - 1; ++i) {
            string match_name;
            Mat match_result;
            drawMatches(features[i].image(), features[i].key_points, features[i + 1].image(), features[i + 1].key_points,
                        matches_for_all[i], match_result);
            match_name = features[i]._image._dir + "match " + features[i]._image.name() + " - " +
                         features[i + 1]._image.name() +
                         ".png";
            imwrite(match_name, match_result);
        }
    }
}

void rc::Matcher::init() {
    i1 = 5, i2 = 1;
    f1 = 0.2f;

    ratio = 0.6;
}

void rc::Matcher::knnMatch(ImageFeature &left, ImageFeature &right, vector<DMatch> &matches) {

    vector<vector<DMatch>> pre_matches;
    if (!load(pre_matches, left, right)) {
        descriptorMatcher->knnMatch(left.descriptor, right.descriptor, pre_matches, 2);
        save(pre_matches, left, right);
    }
    cout << pre_matches.size() << endl;
    //一对匹配 两个DMatch
    for (auto &pre_match : pre_matches) {
        //Ratio Test
        if (!ratioTest(pre_match[0], pre_match[1]))
            continue;

        min_dist = min(min_dist, pre_match[0].distance);
    }

    matches.clear();

    cout << "min distance: " << min_dist << endl;

    for (auto &pre_match : pre_matches) {
        //排除不满足Ratio Test的点和匹配距离过大的点
        if (!ratioTest(pre_match[0], pre_match[1])
            || !distanceTest(pre_match[0]))
            continue;

        //保存匹配点
        matches.push_back(pre_match[0]);
    }
}

void rc::Matcher::oneMatch(ImageFeature &left, ImageFeature &right, vector<DMatch> &matches) {

    vector<DMatch> pre_match;
    if (!load(pre_match, left, right)) {
        descriptorMatcher->match(left.descriptor, right.descriptor, pre_match);
        save(pre_match, left, right);
    }
    cout << pre_match.size() << endl;

    for (int i = 0; i < pre_match.size() - 1; ++i) {
//        if (ratioTest(pre_match[i], pre_match[i + 1]))
        min_dist = min(min_dist, pre_match[i].distance);
    }

    cout << "min distance: " << min_dist << endl;

    for (int i = 0; i < pre_match.size() - 1; ++i) {
        if (
//                ratioTest(pre_match[i], pre_match[i + 1])&&
                distanceTest(pre_match[i])
                )
            matches.emplace_back(pre_match[i]);
    }
}

bool rc::Matcher::ratioTest(DMatch &match1, DMatch &match2) const {
    return match1.distance < ratio * match2.distance;
}

bool rc::Matcher::distanceTest(DMatch &match) const {
    return match.distance < i1 * max(min_dist * i2, f1);
}

string rc::Matcher::saved_path(const ImageFeature &left, const ImageFeature &right) const {
    string path = left._image.directory();
    if (knn)
        path += "knn ";
    path += "match " + left._image.name() + " - " + right._image.name() + ".yml";

    return path;
}

void rc::Matcher::save(vector<vector<DMatch>> &knn_matches, const ImageFeature &left, const ImageFeature &right) {
    cout << "save knn matches to " + saved_path(left, right) << endl;
    FileStorage fs(saved_path(left, right), FileStorage::WRITE);
    if (fs.isOpened()) {
        fs << "Knn Matches" << knn_matches;
        fs.release();
    }
}

void rc::Matcher::save(vector<DMatch> &best_match, const ImageFeature &left, const ImageFeature &right) {
    cout << "save best match to " + saved_path(left, right) << endl;
    FileStorage fs(saved_path(left, right), FileStorage::WRITE);
    if (fs.isOpened()) {
        fs << "Best Match" << best_match;
        fs.release();
    }
}

bool rc::Matcher::load(vector<vector<DMatch>> &knn_matches, const ImageFeature &left, const ImageFeature &right) {
    cout << "load knn matches from " + saved_path(left, right) << endl;
    FileStorage fs(saved_path(left, right), FileStorage::READ);
    if (fs.isOpened()) {
        fs["Knn Matches"] >> knn_matches;
        fs.release();
        return true;
    }
    return false;
}

bool rc::Matcher::load(vector<DMatch> &best_match, const ImageFeature &left, const ImageFeature &right) {
    cout << "load best match from " + saved_path(left, right) << endl;
    FileStorage fs(saved_path(left, right), FileStorage::READ);
    if (fs.isOpened()) {
        fs["Best Match"] >> best_match;
        fs.release();
        return true;
    }
    return false;
}

template<typename T>
bool rc::ReprojectCost::operator()(const T *const intrinsic, const T *const extrinsic, const T *const pos3d,
                                   T *residuals) const {
    const T *r = extrinsic;
    const T *t = &extrinsic[3];

    T pos_proj[3];
    ceres::AngleAxisRotatePoint(r, pos3d, pos_proj);

    // Apply the camera translation
    pos_proj[0] += t[0];
    pos_proj[1] += t[1];
    pos_proj[2] += t[2];

    const T x = pos_proj[0] / pos_proj[2];
    const T y = pos_proj[1] / pos_proj[2];

    const T fx = intrinsic[0];
    const T fy = intrinsic[1];
    const T cx = intrinsic[2];
    const T cy = intrinsic[3];

    // Apply intrinsic
    const T u = fx * x + cx;
    const T v = fy * y + cy;

    residuals[0] = u - T(observation.x);
    residuals[1] = v - T(observation.y);

    return true;
}

rc::Bundler::Bundler()
        : enabled(false) {}

void
rc::Bundler::adjust(Mat &intrinsic, vector<Mat> &extrinsics,
                    vector<ImageFeature> &features, vector<Point3d> &structure) {
    ceres::Problem problem;

    // load extrinsics (rotations and motions)
    for (auto &extrinsic : extrinsics) {
        problem.AddParameterBlock(extrinsic.ptr<double>(), 6);
    }
    // fix the first camera.
    problem.SetParameterBlockConstant(extrinsics[0].ptr<double>());

    // load intrinsic
    problem.AddParameterBlock(intrinsic.ptr<double>(), 4); // fx, fy, cx, cy
    cout << intrinsic << endl;
    // load points
    ceres::LossFunction *loss_function = new ceres::HuberLoss(4);    // loss function make bundle adjustment robuster.
    for (size_t img_idx = 0; img_idx < features.size(); ++img_idx) {
        vector<int> &point3d_ids = features[img_idx].structure_idx;
        vector<KeyPoint> &key_points = features[img_idx].key_points;
        for (size_t point_idx = 0; point_idx < point3d_ids.size(); ++point_idx) {
            int point3d_id = point3d_ids[point_idx];
            if (point3d_id < 0)
                continue;

            Point2d observed = key_points[point_idx].pt;
            // 模板参数中，第一个为代价函数的类型，第二个为代价的维度，剩下三个分别为代价函数第一第二还有第三个参数的维度
            ceres::CostFunction *cost_function = new ceres::AutoDiffCostFunction<ReprojectCost, 2, 4, 6, 3>(
                    new ReprojectCost(observed));

            problem.AddResidualBlock(
                    cost_function,
                    loss_function,
                    intrinsic.ptr<double>(),            // Intrinsic
                    extrinsics[img_idx].ptr<double>(),    // View Rotation and Translation
                    &(structure[point3d_id].x)            // Point in 3D space
            );
        }
    }

    // Solve BA
    ceres::Solver::Options options;
    options.minimizer_progress_to_stdout = false;
    options.logging_type = ceres::SILENT;
    options.num_threads = 4;
    options.preconditioner_type = ceres::JACOBI;
//    options.linear_solver_type = ceres::SPARSE_SCHUR;
//    options.sparse_linear_algebra_library_type = ceres::EIGEN_SPARSE;
    options.linear_solver_type = ceres::DENSE_SCHUR;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    if (!summary.IsSolutionUsable()) {
        std::cout << "Bundle Adjustment failed." << std::endl;
    } else {
        // Display statistics about the minimization
        std::cout << std::endl
                  << "Bundle Adjustment statistics (approximated RMSE):\n"
                  << " #views: " << extrinsics.size() << "\n"
                  << " #residuals: " << summary.num_residuals << "\n"
                  << " Initial RMSE: " << std::sqrt(summary.initial_cost / summary.num_residuals) << "\n"
                  << " Final RMSE: " << std::sqrt(summary.final_cost / summary.num_residuals) << "\n"
                  << " Time (pointCloud): " << summary.total_time_in_seconds << "\n"
                  << std::endl;
    }
    cout << intrinsic << endl;
}
