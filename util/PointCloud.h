//
// Created by hasee on 2021/3/30.
//

#ifndef OPENCV_RECONSTRUCTION_POINTCLOUD_H
#define OPENCV_RECONSTRUCTION_POINTCLOUD_H

#include <opencv2/opencv.hpp>
#include <opencv2/viz.hpp>

using namespace std;
using namespace cv;

class PointCloud {
public:
    enum Status {
        empty,
        valid
    };

    Status status;

    int camera_count = 0, point_count = 0;
    Matx33d K;
    vector<Mat> rotations, motions;
//    vector<Mat> points, colors;
    vector<Point3d> points;
    vector<Vec3b> colors;

    explicit PointCloud();
    explicit PointCloud(Matx33d inputK, vector<Mat>& inputRotations, vector<Mat>& inputMotions, vector<Point3d> &inputPoints, vector<Vec3b> &inputColors);
    explicit PointCloud(const string &file_path);
    void init();
    void show();
    void save(const string &file_name) const;

    friend ostream &operator<<(ostream &out, const PointCloud &s);

private:
    static void KeybdCallback(const viz::KeyboardEvent &keyEvent, void *val);
    static void KeyUpdate(const viz::KeyboardEvent &keyEvent, void *val);

    template<typename T>
    static T to_opposite(T &real);

    template<typename T>
    static void exchange(T &left, T &right);

public:
    static void exchange(Mat &mat, vector<int> &after);
    void testMotion(int num);
};


#endif //OPENCV_RECONSTRUCTION_POINTCLOUD_H
