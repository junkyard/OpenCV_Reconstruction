//
// Created by hasee on 2021/3/7.
//

#ifndef OPENCV_RECONSTRUCTION_RECONSTRUCTION_H
#define OPENCV_RECONSTRUCTION_RECONSTRUCTION_H

#include <QtWidgets>
#include <opencv2/opencv.hpp>

#include "FileNames.h"
#include "PointCloud.h"

using namespace std;
using namespace cv;

namespace rc {

    struct ImageFeature {

        enum Status {
            invalid,
            done
        } status = invalid;

//        图像
        FileName _image;
//        特征点
        vector<KeyPoint> key_points;
//        描述符
        Mat descriptor;
//        颜色
        vector<Vec3b> colors;

        vector<int> structure_idx;

        ImageFeature();

        ImageFeature(const string &imageInput, const Ptr<Feature2D> &featurePtr);

        string saved_path();

        void save(const string& path) const;

        bool load(const string& path);

//        bool empty();

        inline Mat image() const;

        void color_record(Mat &image);
    };

    ostream &operator<<(ostream &out, ImageFeature &feature);

    ostream &operator<<(ostream &out, const ImageFeature::Status &status);

    struct Extractor {

        enum Type {
            SIFT, SURF
        } type;

//        特征检测指针
        Ptr<Feature2D> featurePtr;
        int surf_minHessian;

        Extractor();

        void extract(vector<ImageFeature> &features, const FileNames &imageNames);
    };

    struct Matcher {

        enum Type {
            BruteForce, Flann
        } type;

        int i1, i2;
        float f1;
        DescriptorMatcher *descriptorMatcher;
        float min_dist;
        bool knn;
        bool draw;

        double ratio;

        Matcher();

        void init();

        void match(vector<ImageFeature> &features, vector<vector<DMatch>> &matches_for_all);

        string saved_path(const ImageFeature &left, const ImageFeature& right) const;

        void save(vector<vector<DMatch>> &knn_matches, const ImageFeature& left, const ImageFeature& right);
        void save(vector<DMatch> &best_match, const ImageFeature& left, const ImageFeature& right);

        bool load(vector<vector<DMatch>> &knn_matches, const ImageFeature& left, const ImageFeature& right);
        bool load(vector<DMatch> &best_match, const ImageFeature& left, const ImageFeature& right);

    private:
        void knnMatch(ImageFeature &left, ImageFeature &right, vector<DMatch> &matches);

        void oneMatch(ImageFeature &left, ImageFeature &right, vector<DMatch> &matches);

        bool ratioTest(DMatch &match1, DMatch &match2) const;

        bool distanceTest(DMatch &match) const;
    };

    struct Bundler {
        bool enabled;

        Bundler();

        void adjust(Mat &intrinsic,
                    vector<Mat> &extrinsics,
                    vector<ImageFeature> &features,
                    vector<Point3d> &structure);
    };

    struct ReprojectCost {
        cv::Point2d observation;

        explicit ReprojectCost(cv::Point2d &observation)
                : observation(observation) {}

        template<typename T>
        bool operator()(const T *intrinsic, const T *extrinsic, const T *pos3d, T *residuals) const;
    };

    class Reconstruction {

    public:

        Extractor extractor;

        Matcher matcher;

        Bundler bundler;

        PointCloud run(int camera = 0);

        explicit Reconstruction(FileNames files);

        explicit Reconstruction();

//    PointCloud pointCloud;

    private:
//        文件路径
        FileNames imageNames;
//        所有特征
        vector<ImageFeature> features;
//        所有匹配
        vector<vector<DMatch>> matches_for_all;

        int init_structure_end{};

    public:
        void feature_extract();

        void feature_match();

        bool init_structure(Mat K, PointCloud &cloud);

        void get_objpoints_and_imgpoints(
                vector<DMatch> &matches,
                vector<int> &struct_indices,
                vector<Point3d> &structure,
                vector<KeyPoint> &key_points,
                vector<Point3d> &object_points,
                vector<Point2f> &image_points
        );

        void get_matched_points(
                vector<KeyPoint> &p1,
                vector<KeyPoint> &p2,
                const vector<DMatch> &matches,
                vector<Point2d> &out_p1,
                vector<Point2d> &out_p2
        );

        void get_matched_colors(
                vector<Vec3b> &c1,
                vector<Vec3b> &c2,
                vector<DMatch> &matches,
                vector<Vec3b> &out_c1,
                vector<Vec3b> &out_c2
        );

        bool find_transform(Mat &K, vector<Point2d> &p1, vector<Point2d> &p2, Mat &R, Mat &T, Mat &mask);

        void maskout_points(vector<Point2d> &p1, Mat &mask);

        void maskout_colors(vector<Vec3b> &p1, Mat &mask);

        void reconstruct(Mat &K, Mat &R1, Mat &T1, Mat &R2, Mat &T2, vector<Point2d> &p1, vector<Point2d> &p2,
                         vector<Point3d> &structure);

        void fusion_structure(
                vector<DMatch> &matches,
                ImageFeature &left,
                ImageFeature &right,
                vector<Point3d> &structure,
                vector<Point3d> &next_structure,
                vector<Vec3b> &colors,
                vector<Vec3b> &next_colors
        );
    };

}

#endif //OPENCV_RECONSTRUCTION_RECONSTRUCTION_H
