//
// Created by hasee on 2021/3/21.
//

#ifndef OPENCV_RECONSTRUCTION_FILENAMES_H
#define OPENCV_RECONSTRUCTION_FILENAMES_H

#include <QtWidgets>

using namespace std;

QT_BEGIN_NAMESPACE
//class QString;
QT_END_NAMESPACE

struct FileName {
    string _path;
    string _dir;
    string _name;

public:
    explicit FileName();
    explicit FileName(string file_path);
    explicit FileName(const QString& qString_path);

    void init();

    string name(bool extension=false) const;
    string path() const;
    string directory() const;
    QString Qname(bool extension=false) const;
    QString Qpath() const;
};

class FileNames {

private:

    vector<FileName> _files;

    void clear();

public:
    FileNames();
    explicit FileNames(const QList<QString>& files);
    void selectFiles(const QList<QString>& files);
    bool empty();
    unsigned long long size();

    vector<string> paths();
    vector<string> names();
    vector<FileName> files() const;

    FileNames &operator+=(const FileNames& other);

    FileNames operator+(const FileNames& other);

    friend ostream &operator<<(ostream &out, const FileNames &files);
};


#endif //OPENCV_RECONSTRUCTION_FILENAMES_H
