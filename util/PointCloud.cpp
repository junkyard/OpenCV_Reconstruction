//
// Created by hasee on 2021/3/30.
//

#include "PointCloud.h"

ostream &operator<<(ostream &out, const PointCloud &s) {
    return out
            << "camera count: " << s.camera_count << endl
            << "point count: " << s.point_count << endl;
}

PointCloud::PointCloud()
: status(empty), rotations(), motions(), points(), colors(){}

PointCloud::PointCloud(Matx33d inputK, vector<Mat>& inputRotations, vector<Mat>& inputMotions, vector<Point3d> &inputPoints, vector<Vec3b> &inputColors)
: status(valid), K(inputK), camera_count(inputMotions.size()), point_count(inputPoints.size()), rotations(inputRotations), motions(inputMotions), points(inputPoints), colors(inputColors) {}

PointCloud::PointCloud(const string &file_path)
: status(valid), rotations(), motions(), points(), colors() {

    FileStorage fs(file_path, FileStorage::READ);

    if (fs.isOpened()) {
        cout << file_path << endl;

        fs["Camera Count"] >> camera_count;
        fs["Point Count"] >> point_count;
        fs["K"] >> K;
        fs["Rotations"] >> rotations;
        fs["Motions"] >> motions;
        fs["Points"] >> points;
        fs["Colors"] >> colors;

        fs.release();
    } else {
        cout << "yaml file not found" << endl;
    }
}

void PointCloud::save(const string &file_name) const {

    FileStorage fs(file_name, FileStorage::WRITE);

    fs << "Camera Count" << camera_count;
    fs << "Point Count" << point_count;
    fs << "K" << K;
    fs << "Rotations" << rotations;
    fs << "Motions" << motions;
    fs << "Points" << points;
    fs << "Colors" << colors;

    fs.release();
}

void PointCloud::init() {
    rotations.resize(camera_count);
    motions.resize(camera_count);
    points.resize(point_count);
    colors.resize(point_count);
}

void PointCloud::show() {

    auto window = viz::Viz3d("Cloud Frame");
    window.setWindowSize(Size(800, 600));
    window.setWindowPosition(Point(150, 150));
    window.setBackgroundColor();

    cout << "Recovering points  ... ";
//    vector<Vec3f> point_cloud_est;
//    vector<Vec3b> point_color;
//    for (auto &p : points)
//        point_cloud_est.emplace_back(Point3f(p));
//    for (auto &c : colors)
//        point_color.emplace_back(c);
    cout << "[DONE]" << endl;

    cout << "Recovering cameras ... ";

    vector<Affine3d> path;

    for (size_t i = 0; i < camera_count; ++i) {
        path.emplace_back(Affine3d(rotations[i], motions[i]));
    }

//    for (size_t i = 0; i < camera_count; ++i) {

//        Mat &T = motions[i];
//
//        for (int j = 0; j < 2; ++j) {
//        for (int j = 0; j < 2; ++j) {
//            exchange(T.at<double>(j), T.at<double>(j + 1));
//        }

//        path.emplace_back(Affine3d(rotations[i], motions[i]));
//    }
    cout << "[DONE]" << endl;

    auto path_it = path.begin();

    if ( !points.empty() )
    {
        cout << "Rendering points   ... ";
        viz::WCloud cloud_widget(points, colors);
        window.showWidget("point_cloud", cloud_widget);
        cout << "[DONE]" << endl;
    }
    else
    {
        cout << "Cannot render points: Empty point cloud" << endl;
    }

    if ( !path.empty() )
    {
        cout << "Rendering Cameras  ... ";

        window.showWidget("cameras_frames_and_lines", viz::WTrajectory(path, viz::WTrajectory::BOTH, 0.1, viz::Color::green()));
        window.showWidget("cameras_frustums", viz::WTrajectoryFrustums(path, K, 0.1, viz::Color::yellow()));

//        cout << _path[0].matrix << endl;
//        cout << path_it++->matrix << endl;
//        cout << path_it->matrix << endl;
        window.setViewerPose(path[0]);
//        window.setViewerPose(path_it->matrix);

        cout << "[DONE]" << endl;
    }
    else
    {
        cout << "Cannot render the cameras: Empty _path" << endl;
    }

    cout << endl << "Press 'q' to close each windows ... " << endl;


//    int pose = 0;
    //注册键盘指令
//    window.registerKeyboardCallback(KeybdCallback, &pose);
//    window.registerKeyboardCallback(KeyUpdate, {&window, &pose});

//    window.setViewerPose(path[pose]);

    window.spin();
}

void PointCloud::KeybdCallback(const viz::KeyboardEvent &keyEvent, void *val) {

    int *pose = (int*) val;

    if (keyEvent.action == viz::KeyboardEvent::KEY_DOWN) {
        switch (keyEvent.code) {
            case 'a':
                cout << "press a" << endl;
                break;
            case 'x':
                *pose += 1;
                cout << *pose << endl;
                break;
        }
    }
}

void PointCloud::KeyUpdate(const viz::KeyboardEvent &keyEvent, void *val) {
    auto win = (viz::Viz3d*) val;

    if (keyEvent.action == viz::KeyboardEvent::KEY_DOWN) {
        switch (keyEvent.code) {
            case 'x':
                win->spinOnce();
                break;
        }
    }
}

template<typename T>
T PointCloud::to_opposite(T &real) {
    real = -real;
    return real;
}

template<typename T>
void PointCloud::exchange(T &left, T &right) {
    T temp = left;
    left = right;
    right = temp;
}

void PointCloud::exchange(Mat &mat, vector<int> &after) {
    vector<double> temp;
    for (int i = 0; i < after.size(); ++i) {
        temp.push_back(mat.at<double>(i));
    }
    for (int i = 0; i < after.size(); ++i) {
        mat.at<double>(i) = temp[after[i]];
    }
}

void PointCloud::testMotion(int num) {
    switch (num) {
        case 2:
            for (int i = 0; i < 8; ++i) {
                for (int j = i + 1; j < 9; ++j) {
                    for (auto &motion: motions) {
                        swap(motion.at<double>(i), motion.at<double>(j));
                    }
                    cout << "exchange " << i << "-" << j << endl;
                    string name;
                    sprintf(name.data(), "../image/test/motion exchange %d-%d.yml", i, j);
                    save(name);
                    for (auto &motion: motions) {
                        swap(motion.at<double>(j), motion.at<double>(i));
                    }
                }
            }
            break;
        case 3:
            for (int i = 0; i < 2; ++i) {
                for (int j = i + 1; j < 3; ++j) {
                    for (auto &motion: motions) {
                        for (int k = 0; k < 3; ++k) {
                            swap(motion.at<double>(i, k), motion.at<double>(j, k));
                        }
                    }
                    cout << "exchange rows " << i << "-" << j << endl;
                    string name;
                    sprintf(name.data(), "../image/test/motion exchange rows %d-%d.yml", i, j);
                    save(name);
                    for (auto &motion: motions) {
                        for (int k = 0; k < 3; ++k) {
                            swap(motion.at<double>(j, k), motion.at<double>(i, k));
                        }
                    }

                    for (auto &motion: motions) {
                        for (int k = 0; k < 3; ++k) {
                            swap(motion.at<double>(k, i), motion.at<double>(k, i));
                        }
                    }
                    cout << "exchange cols " << i << "-" << j << endl;
                    sprintf(name.data(), "../image/test/motion exchange cols %d-%d.yml", i, j);
                    save(name);
                    for (auto &motion: motions) {
                        for (int k = 0; k < 3; ++k) {
                            swap(motion.at<double>(k, j), motion.at<double>(k, i));
                        }
                    }
                }
            }
            break;
        default:
            break;
    }
}
