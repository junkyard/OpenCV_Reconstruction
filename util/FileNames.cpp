//
// Created by hasee on 2021/3/21.
//

#include "FileNames.h"

#include <utility>
#include <QDebug>

#include <iostream>
#include <regex>

FileName::FileName(string file_path)
: _path(std::move(file_path)) {
    init();
}

FileName::FileName(const QString& qString_path)
: _path(qString_path.toStdString()) {
    init();
}

FileName::FileName() = default;

void FileName::init() {
    unsigned long long pos = _path.find_last_of('/');
    _dir = _path.substr(0, pos + 1);
    _name = _path.substr(pos + 1, _path.size());
}

void FileNames::selectFiles(const QList<QString> &files) {

    clear();

    for (auto &qFile : files) {
        FileName file(qFile);
        _files.emplace_back(file);
    }
}

vector<string> FileNames::paths() {
    vector<string> p;
    for (auto &file : _files) {
        p.emplace_back(file._path);
    }
    return p;
}

vector<string> FileNames::names() {
    vector<string> n;
    for (auto &file : _files) {
        n.emplace_back(file._name);
    }
    return n;
}

void FileNames::clear() {
    _files.clear();
}

unsigned long long FileNames::size() {
    return _files.size();
}

vector<FileName> FileNames::files() const {
    return _files;
}

bool FileNames::empty() {
    return _files.empty();
}

FileNames::FileNames() = default;

FileNames::FileNames(const QList<QString>& files) {
    selectFiles(files);
}

FileNames &FileNames::operator+=(const FileNames &other) {
    for (auto &file : other._files) {
        _files.emplace_back(file);
    }
    return *this;
}

FileNames FileNames::operator+(const FileNames &other) {
    *this += other;
    return *this;
}

ostream &operator<<(ostream &out, const FileNames &files) {
    for (auto &file : files._files) {
        out << file._path << endl;
    }
    return out;
}

string FileName::name(bool extension) const {
    if (!extension)
        return _name.substr(0, _name.find_last_of('.'));
    return _name;
}

string FileName::path() const {
    return _path;
}

QString FileName::Qname(bool extension) const {
    if (!extension)
        return QString(_name.substr(0, _name.find_last_of('.')).data());
    return QString(_name.data());
}

QString FileName::Qpath() const {
    return QString(_path.data());
}

string FileName::directory() const {
    return _dir;
}
