//
// Created by hasee on 2021/2/28.
//

#include <QtWidgets>
#include <QDebug>
#include <iostream>
#include "AppDialog.h"
#include "../../util/Reconstruction.h"

AppDialog::AppDialog()
: imageLabel(new QLabel), scrollArea(new QScrollArea)
{
    createMenu();
//    createHorizontalGroupBox();

    auto *mainLayout = new QVBoxLayout;

    mainLayout->setMenuBar(menuBar);
    mainLayout->addWidget(horizontalGroupBox);

    setLayout(mainLayout);
    setWindowTitle("3D Reconstruction");
}

void AppDialog::createMenu() {
    menuBar = new QMenuBar;
    fileMenu = new QMenu(tr("&File"), this);
    showImageAction = fileMenu->addAction(tr("显示图片"), this, &AppDialog::showImage);
    loadImagesAction = fileMenu->addAction(tr("Load Images"));
    saveStructureAction = fileMenu->addAction(tr("Save PointCloud"));
    exitAction = fileMenu->addAction(tr("&Exit"));
    menuBar->addMenu(fileMenu);

    connect(exitAction, &QAction::triggered, this, &QDialog::accept);

    connect(loadImagesAction, &QAction::triggered, this, [=] {
        auto *fileDialog = new QFileDialog(this);
        fileDialog->setWindowTitle(QStringLiteral("选择图片"));
        fileDialog->setDirectory(R"(G:\OneDrive - cjlu.edu.cn\3D Reconstruction\OpenCV_Reconstruction\image)");
        fileDialog->setNameFilter(tr("Images (*.png *.xpm *.jpg *.gif)"));
        fileDialog->setFileMode(QFileDialog::ExistingFiles);
        fileDialog->setViewMode(QFileDialog::Detail);
//        QStringList fileNames;
        if (fileDialog->exec()) {
            fileNames = fileDialog->selectedFiles();
            fileDialog->directoryUrl();
            auto img = new QImage;
            qDebug() << fileNames << Qt::endl;

//            for (auto &f : fileNames) {
//                auto myImage = QImage(f);
//                auto imgLabel = new QLabel();
//                imgLabel->setPixmap(QPixmap(f));
//                layout->addWidget(imgLabel);
//
//            }
        }
//        Reconstruction::run(fileNames);
    });

    connect(saveStructureAction, &QAction::triggered, this, [=] {
        Reconstruction::run(fileNames);
    });
}

void AppDialog::loadImages() {
    auto *fileDialog = new QFileDialog(this);
    fileDialog->setWindowTitle(QStringLiteral("选择图片"));
    fileDialog->setDirectory(R"(G:\OneDrive - cjlu.edu.cn\3D Reconstruction\OpenCV_Reconstruction\image)");
    fileDialog->setNameFilter(tr("Images (*.png *.xpm *.jpg *.gif)"));
    fileDialog->setFileMode(QFileDialog::ExistingFiles);
    fileDialog->setViewMode(QFileDialog::Detail);
    if (fileDialog->exec()) {
        fileNames = fileDialog->selectedFiles();
        fileDialog->directoryUrl();
        auto img = new QImage;
        qDebug() << fileNames << Qt::endl;
    }
}

void AppDialog::createHorizontalGroupBox() {
    horizontalGroupBox = new QGroupBox(tr("Horizontal layout"));
    auto *layout = new QHBoxLayout;

    horizontalGroupBox->setLayout(layout);
}

void AppDialog::showImage() {
    auto *fileDialog = new QFileDialog(this);
    fileDialog->setWindowTitle("选择图片");
    fileDialog->setDirectory(R"(G:\OneDrive - cjlu.edu.cn\3D Reconstruction\OpenCV_Reconstruction\image)");
    fileDialog->setNameFilter(tr("Images (*.png *.xpm *.jpg *.gif)"));
    fileDialog->setViewMode(QFileDialog::Detail);
    if (fileDialog->exec()) {
        fileNames = fileDialog->selectedFiles();
        Mat image = imread(fileNames[0].toStdString());
        imshow("", image);
        cv::waitKey();
    }
}


