//
// Created by hasee on 2021/2/28.
//

#ifndef OPENCV_RECONSTRUCTION_APPDIALOG_H
#define OPENCV_RECONSTRUCTION_APPDIALOG_H

#include <QDialog>
QT_BEGIN_NAMESPACE
class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QMenuBar;
class QPushButton;
class QTextEdit;
QT_END_NAMESPACE

class AppDialog : public QDialog{

    Q_OBJECT

public:
    AppDialog();

private:
    QList<QString> fileNames;

    void createMenu();
    void createHorizontalGroupBox();

    QMenuBar *menuBar;

    QMenu *fileMenu;
    QAction *showImageAction;
    QAction *loadImagesAction;
    QAction *saveStructureAction;
    QAction *exitAction;
    QGroupBox *horizontalGroupBox;


private slots:
    void showImage();
    void loadImages();
};


#endif //OPENCV_RECONSTRUCTION_APPDIALOG_H
