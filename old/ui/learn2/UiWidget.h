//
// Created by hasee on 2021/3/20.
//

#ifndef OPENCV_RECONSTRUCTION_UIWIDGET_H
#define OPENCV_RECONSTRUCTION_UIWIDGET_H

#pragma execution_character_set("utf-8")

#include <QtWidgets/QMainWindow>
#include <opencv2/opencv.hpp>

using namespace cv;

class UiWidget : public QMainWindow {

    Q_OBJECT

public:
    UiWidget(QWidget *parent = Q_NULLPTR);
    Mat srcImg, grayImg;

private:
    Ui::UiWidget ui;

private slots:
    void on_checkBox_clicked();
    void on_pushButton_clicked();
};


#endif //OPENCV_RECONSTRUCTION_UIWIDGET_H
