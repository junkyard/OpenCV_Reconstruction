//
// Created by hasee on 2021/2/19.
//
#include <opencv2/viz.hpp>
#include <iostream>

#include "load_cloud.h"

using namespace std;
using namespace cv;

StructureOld myStructure;

void Structure::init() {
    rotations.resize(camera_count);
    motions.resize(camera_count);
    points.resize(point_count);
    colors.resize(point_count);
}

Structure load_yaml(const std::string &yaml_path) {

    FileStorage fs(yaml_path, FileStorage::READ);

    if (fs.isOpened()) {
        cout << yaml_path << endl;

        StructureOld s;

        fs["Camera Count"] >> s.camera_count;
        fs["Point Count"] >> s.point_count;
        fs["K"] >> s.K;

        s.init();

        fs["Rotations"] >> s.rotations;
        fs["Motions"] >> s.motions;
        fs["Points"] >> s.points;
        fs["Colors"] >> s.colors;

        cout << s.rotations[0].rows << endl;

        fs.release();
        return s;
    } else {
        cout << "yaml not found" << endl;
        exit(1);
    }
}

void show_structure(string yaml_path) {
//    PointCloud pointCloud = load_yaml(yaml_path);
    myStructure = load_yaml(yaml_path);
    StructureOld s = myStructure;

    viz::Viz3d window("Coordinate Frame");
    window.setWindowSize(Size(800, 600));
    window.setWindowPosition(Point(150, 150));
    window.setBackgroundColor();

    cout << "Recovering points  ... ";
    vector<Vec3f> point_cloud_est;
    vector<Vec3b> point_color;
    for (auto &p : s.points)
        point_cloud_est.emplace_back(p);
    for (auto &c : s.colors)
        point_color.emplace_back(c);
    cout << "[DONE]" << endl;

    cout << "Recovering cameras ... ";
    vector<Affine3d> path;
    for (size_t i = 0; i < s.camera_count; ++i)
        path.emplace_back(Affine3d(s.rotations[i], s.motions[i]));
    cout << "[DONE]" << endl;

    if ( !point_cloud_est.empty() )
    {
        cout << "Rendering points   ... ";
        viz::WCloud cloud_widget(point_cloud_est, point_color);
        window.showWidget("point_cloud", cloud_widget);
        cout << "[DONE]" << endl;
    }
    else
    {
        cout << "Cannot render points: Empty pointcloud" << endl;
    }

    if ( !path.empty() )
    {
        cout << "Rendering Cameras  ... ";

        window.showWidget("cameras_frames_and_lines", viz::WTrajectory(path, viz::WTrajectory::BOTH, 0.1, viz::Color::green()));
        window.showWidget("cameras_frustums", viz::WTrajectoryFrustums(path, s.K, 0.1, viz::Color::yellow()));

        window.setViewerPose(path[0]);

        cout << "[DONE]" << endl;
    }
    else
    {
        cout << "Cannot render the cameras: Empty _path" << endl;
    }

    cout << endl << "Press 'q' to close each windows ... " << endl;

    window.spin();

}

void save_as(const string &saved_name) {
    vector<Mat> colorPoints(myStructure.points.size());
    for (auto &cp : colorPoints) {
        cp.at<Vec3b>();
    }
    viz::writeCloud(saved_name, myStructure.points, myStructure.colors);
}
