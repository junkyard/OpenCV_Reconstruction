//
// Created by hasee on 2021/3/23.
//

#ifndef OPENCV_RECONSTRUCTION_LOAD_CLOUD_H
#define OPENCV_RECONSTRUCTION_LOAD_CLOUD_H

#include "opencv2/opencv.hpp"

struct StructureOld{

    int camera_count = 0, point_count = 0;
    cv::Matx33d K;
    std::vector<cv::Mat> rotations, motions, points, colors;

    void init();
};

StructureOld load_yaml(const std::string &yaml_path);

void show_structure(std::string yaml_path="../image/structure.yml");

void save_as(const std::string &saved_name);

#endif //OPENCV_RECONSTRUCTION_LOAD_CLOUD_H
