#include "ui/MainWindow.h"
#include <QApplication>

int main(int argc, char* argv[]) {
    QApplication app(argc, argv);
    MainWindow rv;
    rv.show();
    return QApplication::exec();
}
