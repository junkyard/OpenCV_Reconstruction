//
// Created by hasee on 2021/3/20.
//

#include "MainWindow.h"
#include "../util/Reconstruction.h"
#include <QtWidgets>
#include <QDebug>
#include <iostream>

using namespace rc;

MainWindow::MainWindow(QWidget *parent):
        QMainWindow(parent), seniorWindow(new SeniorWindow(this)),
        imageLabel(new QLabel), scrollArea(new QScrollArea), listWidget(), pointCloud()
{
    initListWidget();

    createMenus();

    resize(QGuiApplication::primaryScreen()->availableSize() * 2 / 5);
}

//创建菜单
void MainWindow::createMenus() {

    fileMenu = menuBar()->addMenu("文件");
    openAct = fileMenu->addAction("打开图片", this, &MainWindow::open);
    addAct = fileMenu->addAction("添加图片", this, &MainWindow::add);
    exitAct = fileMenu->addAction("退出", this, &QWidget::close);

    operatorMenu = menuBar()->addMenu("重建");
    computeAct = operatorMenu->addAction("计算", this, &MainWindow::compute);

    viewMenu = menuBar()->addMenu("点云");
    loadAct = viewMenu->addAction("加载", this, &MainWindow::load);
    saveAct = viewMenu->addAction("保存", this, &MainWindow::save);

    seniorAct = menuBar()->addAction("高级参数", this, &MainWindow::startSeniorWindow);
}

//打开文件
void MainWindow::open() {
    auto fileDialog = new QFileDialog(this, "选择图片");
    fileDialog->setDirectory(R"(G:\OneDrive - cjlu.edu.cn\3D Reconstruction\OpenCV_Reconstruction\image)");
    fileDialog->setNameFilter("Images (*.png *.jpg)");
    fileDialog->setFileMode(QFileDialog::ExistingFiles);
    fileDialog->setViewMode(QFileDialog::Detail);
    if (fileDialog->exec()) {
        imageFiles.selectFiles(
                fileDialog->selectedFiles());
        createListWidget();
    }
}

void MainWindow::add() {
    auto fileDialog = new QFileDialog(this, "添加图片");
    fileDialog->setDirectory(R"(G:\OneDrive - cjlu.edu.cn\3D Reconstruction\OpenCV_Reconstruction\image)");
    fileDialog->setNameFilter("Images (*.png *.jpg)");
    fileDialog->setFileMode(QFileDialog::ExistingFiles);
    fileDialog->setViewMode(QFileDialog::Detail);
    if (fileDialog->exec()) {
        extraFiles = FileNames(fileDialog->selectedFiles());
        imageFiles += extraFiles;
        updateListWidget();
    }
}

//计算重建
void MainWindow::compute() {
    if (imageFiles.empty()) {
        QMessageBox::information(nullptr, "空的图像", "请先选择重建照片", QMessageBox::Ok);
    } else {
        rc::Reconstruction r(imageFiles);
        seniorWindow.configure(r);
        pointCloud = r.run(seniorWindow.camera_matrix);
        if (pointCloud.status == PointCloud::empty)
            QMessageBox::information(nullptr, "未初始化结构", "图像不适合初始化重建");
    }
}

//加载点云
void MainWindow::load() {
    auto *fileDialog = new QFileDialog(this, "选择点云");
    fileDialog->setDirectory(R"(G:\OneDrive - cjlu.edu.cn\3D Reconstruction\OpenCV_Reconstruction\image)");
    fileDialog->setNameFilter("*.yml");
    fileDialog->setViewMode(QFileDialog::Detail);
    if (fileDialog->exec()) {
        string path = fileDialog->selectedFiles()[0].toStdString();
        pointCloud = PointCloud(path);
        pointCloud.show();
    }
}

void MainWindow::save() {

    if (pointCloud.point_count == 0) {
        QMessageBox::information(nullptr, "保存失败", "未生成点云", QMessageBox::Ok);
        return;
    }

    string path = QFileDialog::getSaveFileName(this, "保存点云", R"(G:\OneDrive - cjlu.edu.cn\3D Reconstruction\OpenCV_Reconstruction\image)", "*.yml").toStdString();

    cout << "saved file: " << path << endl;
    pointCloud.save(path);

}

//初始化列表
void MainWindow::initListWidget() {
    const auto ICON_SIZE = QSize(210, 210);
//    const auto CELL_SIZE = QSize(220, 220);
    listWidget = new QListWidget(this);
    setCentralWidget(listWidget);
    listWidget->setIconSize(ICON_SIZE);
    listWidget->setResizeMode(QListView::Adjust);
    listWidget->setViewMode(QListView::IconMode);
    listWidget->setMovement(QListView::Static);
    listWidget->setSpacing(10);
    listWidget->horizontalScrollBar()->setDisabled(true);
    imageLabel = new QLabel;
    imageLabel->setWindowTitle("Title");
}

//列表加载图片
void MainWindow::createListWidget() {

    listWidget->clear();

    for (auto &file : imageFiles.files()) {
        listWidget->addItem(new QListWidgetItem(QIcon(file.Qpath()), file.Qname()));
    }
}

void MainWindow::updateListWidget() {
    for (auto &file : extraFiles.files()) {
        listWidget->addItem(new QListWidgetItem(QIcon(file.Qpath()), file.Qname()));
    }
}

void MainWindow::startSeniorWindow() {
    seniorWindow.show();
}

