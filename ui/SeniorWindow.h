//
// Created by hasee on 2021/4/16.
//

#ifndef OPENCV_RECONSTRUCTION_SENIORWINDOW_H
#define OPENCV_RECONSTRUCTION_SENIORWINDOW_H

#include <QDialog>
#include "../util/Reconstruction.h"

class SeniorWindow : public QDialog{
    Q_OBJECT

public:
    explicit SeniorWindow(QWidget *parent = 0);

    int camera_matrix;

    rc::Extractor extractor;
    rc::Matcher matcher;
    rc::Bundler bundler;

    void configure(rc::Reconstruction &r);

public slots:
    void getFeaturePtr();
    void getMatcherTypes();
    void toggleBundle();
    void toggleKnn();
    void toggleDraw();

    rc::Matcher getParameter();

private:
    QGroupBox *radioWidget{};
    QButtonGroup *featurePtrGroup{}, *matchTypeGroup{}, *knnGroup{}, *drawGroup{}, *bundleGroup{};
    QList<QRadioButton *> featureBtn, matchBtn, knnBtn, drawBtn, bundleBtn;

    QGroupBox *parameterWidget{};
    QLineEdit *cameraLine, *ratioLine;
    QList<QLineEdit*> parameters;

    void createRadioWidget();
    void createParameterWidget();

    void connectGroupButtons(const QList<QRadioButton *> &buttons, QButtonGroup *group, const function<void()> &func);
    static void addGroupButtonToLayout(QLayout *layout, QButtonGroup *group, const QList<QRadioButton *> &buttons);

};


#endif //OPENCV_RECONSTRUCTION_SENIORWINDOW_H
