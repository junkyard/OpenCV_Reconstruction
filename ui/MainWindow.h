//
// Created by hasee on 2021/3/20.
//

#ifndef OPENCV_RECONSTRUCTION_MAINWINDOW_H
#define OPENCV_RECONSTRUCTION_MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include "../util/FileNames.h"
#include "../util/PointCloud.h"
#include "SeniorWindow.h"

QT_BEGIN_NAMESPACE
class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;
class QListView;
class QListWidget;
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

private slots:
    void open();
    void add();
    void compute();
    void load();
    void save();
    void startSeniorWindow();

private:
    void createMenus();
    void initListWidget();
    void createListWidget();
    void updateListWidget();

    FileNames imageFiles, extraFiles;
    QMenu *fileMenu{}, *operatorMenu{}, *viewMenu{};
    QAction *openAct{}, *addAct{}, *exitAct{}, *computeAct{}, *loadAct{}, *saveAct{}, *seniorAct{};
    QImage image;
    QLabel *imageLabel;
    QScrollArea *scrollArea;
    QListWidget *listWidget;
    double scaleFactor = 1;

    SeniorWindow seniorWindow;

    PointCloud pointCloud;
};


#endif //OPENCV_RECONSTRUCTION_MAINWINDOW_H
