//
// Created by hasee on 2021/4/16.
//

#include "SeniorWindow.h"

SeniorWindow::SeniorWindow(QWidget *parent):
camera_matrix(0), extractor(), matcher()
{
    auto *mainLayout = new QVBoxLayout;

    createRadioWidget();
    createParameterWidget();

    mainLayout->addWidget(radioWidget);
    mainLayout->addWidget(parameterWidget);

    setLayout(mainLayout);
    setWindowTitle("参数面板");

    resize(200, 300);
}


void SeniorWindow::connectGroupButtons(const QList<QRadioButton *> &buttons, QButtonGroup *group, const function<void()> &func) {
    for (auto &btn : buttons) {
//        connect(btn, SIGNAL(clicked(bool)), this, SLOT(slotFunc));
        group->addButton(btn);
    }
}

void SeniorWindow::addGroupButtonToLayout(QLayout *layout, QButtonGroup *group, const QList<QRadioButton *> &buttons) {
    for (int i = 0; i < buttons.size(); ++i) {
        group->addButton(buttons[i], i);
        layout->addWidget(buttons[i]);
    }
}

void SeniorWindow::createRadioWidget() {
    radioWidget = new QGroupBox("特征操作");

    auto *layout = new QFormLayout;
    QList<QHBoxLayout *> layoutH = {
            new QHBoxLayout,
            new QHBoxLayout,
            new QHBoxLayout,
            new QHBoxLayout,
    };

//    QList<QHBoxLayout*> layoutH(4);

    radioWidget->setLayout(layout);

    featurePtrGroup = new QButtonGroup(this);
    featureBtn = {
            new QRadioButton("SIFT"),
            new QRadioButton("SURF")
    };
//    connectGroupButtons(featureBtn, featurePtrGroup, [this] { getFeaturePtr(); });
    for (auto &btn : featureBtn) {
        connect(btn, SIGNAL(clicked(bool)), this, SLOT(getFeaturePtr()));
    }

    matchTypeGroup = new QButtonGroup(this);
    matchBtn = {
            new QRadioButton("Brute Match"),
            new QRadioButton("Flann Match")
    };
    for (auto &btn : matchBtn) {
        connect(btn, SIGNAL(clicked(bool)), this, SLOT(getMatcherTypes()));
    }

    knnGroup = new QButtonGroup(this);
    knnBtn = {
            new QRadioButton("on"),
            new QRadioButton("off")
    };

    for (auto &btn : knnBtn) {
        connect(btn, SIGNAL(clicked(bool)), this, SLOT(toggleKnn()));
    }

    drawGroup = new QButtonGroup(this);
    drawBtn = {
            new QRadioButton("on"),
            new QRadioButton("off")
    };

    for (auto &btn : drawBtn) {
        connect(btn, SIGNAL(clicked(bool)), this, SLOT(toggleDraw()));
    }

    bundleGroup = new QButtonGroup(this);
    bundleBtn = {
            new QRadioButton("on"),
            new QRadioButton("off")
    };

    for (auto &btn : bundleBtn) {
        connect(btn, SIGNAL(clicked(bool)), this, SLOT(toggleBundle()));
    }


    layout->addRow("特征检测指针", layoutH[0]);
    addGroupButtonToLayout(layoutH[0], featurePtrGroup, featureBtn);

    layout->addRow("特征匹配器", layoutH[1]);
    addGroupButtonToLayout(layoutH[1], matchTypeGroup, matchBtn);

    layout->addRow("knn匹配", layoutH[2]);
    addGroupButtonToLayout(layoutH[2], knnGroup, knnBtn);

    layout->addRow("画出匹配", layoutH[3]);
    addGroupButtonToLayout(layoutH[3], drawGroup, drawBtn);

//    layout->addRow("光束法平差", layoutH[3]);
//    addGroupButtonToLayout(layoutH[3], bundleGroup, bundleBtn);

    featureBtn[0]->setChecked(true);
    matchBtn[1]->setChecked(true);
    knnBtn[0]->setChecked(true);
    drawBtn[1]->setChecked(true);
    bundleBtn[1]->setChecked(true);
}

void SeniorWindow::createParameterWidget() {
    parameterWidget = new QGroupBox("参数调整");
    auto *layout = new QVBoxLayout;
    parameterWidget->setLayout(layout);

    QList<QLayout *> layoutH = {
            new QHBoxLayout,
            new QHBoxLayout
    };

    for (auto h : layoutH) {
        layout->addLayout(h);
    }

    layoutH[0]->addWidget(new QLabel("K: "));
    cameraLine = new QLineEdit("0");
    cameraLine->setValidator(new QIntValidator);
    layoutH[0]->addWidget(cameraLine);

    layoutH[0]->addWidget(new QLabel("Ratio Test: "));
    ratioLine = new QLineEdit("0.6");
    ratioLine->setValidator(new QDoubleValidator);
    layoutH[0]->addWidget(ratioLine);

    QList<QLabel*> labels = {
            new QLabel("distance < "),
            new QLabel(" * max(min_dist * "),
            new QLabel(", "),
            new QLabel(")")
    };

    parameters = {
            new QLineEdit("5"),
            new QLineEdit("1"),
            new QLineEdit("0.2")
    };

    for (int i = 0; i < 2; i++) {
        parameters[i]->setValidator(new QIntValidator);
    }
    parameters[2]->setValidator(new QDoubleValidator);

    layoutH[1]->addWidget(labels[0]);
    layoutH[1]->addWidget(parameters[0]);
    layoutH[1]->addWidget(labels[1]);
    layoutH[1]->addWidget(parameters[1]);
    layoutH[1]->addWidget(labels[2]);
    layoutH[1]->addWidget(parameters[2]);
    layoutH[1]->addWidget(labels[3]);
}

void SeniorWindow::getFeaturePtr() {

    cout << "Feature Ptr has changed to ";
    switch (featurePtrGroup->checkedId()) {
        case 0:
            cout << "SIFT";
            extractor.type = extractor.SIFT;
            break;
        case 1:
            cout << "SURF";
            extractor.type = extractor.SURF;
            break;
    }
    cout << endl;
}

void SeniorWindow::getMatcherTypes() {

    cout << "Matcher has changed to ";
    switch (matchTypeGroup->checkedId()) {
        case 0:
            cout << "Brute KNN Matcher";
            matcher.type = matcher.BruteForce;
            break;
        case 1:
            cout << "Flann Matcher";
            matcher.type = matcher.Flann;
            break;
    }
    cout << endl;
}

void SeniorWindow::toggleBundle() {
    switch (bundleGroup->checkedId()) {
        case 0:
            cout << "Bundle Adjustment On";
            bundler.enabled = true;
            break;
        case 1:
            cout << "Bundle Adjustment Off";
            bundler.enabled = false;
            break;
    }
    cout << endl;
}

void SeniorWindow::toggleKnn() {
    switch (knnGroup->checkedId()) {
        case 0:
            cout << "2 Matches output one time";
            matcher.knn = true;
            break;
        case 1:
            cout << "Best Match output one time";
            matcher.knn = false;
            break;
    }
    cout << endl;
}

void SeniorWindow::toggleDraw() {
    switch (drawGroup->checkedId()) {
        case 0:
            cout << "Enable Draw Matches";
            matcher.draw = true;
            break;
        case 1:
            cout << "Disable Draw Matches";
            matcher.draw = false;
            break;
    }
    cout << endl;
}


rc::Matcher SeniorWindow::getParameter() {
    camera_matrix = cameraLine->text().toInt();
    cout << "camera: " << camera_matrix << endl;
    matcher.ratio = ratioLine->text().toDouble();
    cout << "ratio test level is set to " << matcher.ratio << endl;
    matcher.i1 = parameters[0]->text().toInt();
    matcher.i2 = parameters[1]->text().toInt();
    matcher.f1 = parameters[2]->text().toFloat();
    cout << "distance test parameters are set to "<< matcher.i1 << " " << matcher.i2 << " " << matcher.f1 << endl;
    return matcher;
}

void SeniorWindow::configure(rc::Reconstruction &r) {
    getParameter();
    r.extractor = extractor;
    r.matcher = matcher;
}